package com.levelup.examples;

import java.util.Random;

public class Field {

    private final int[][] FIELD = new int[10][10];

    private final Random RANDOM = new Random();

    public synchronized void generate() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                FIELD[i][j] = RANDOM.nextInt(10);
            }
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("notify");
        notify();

    }

    public synchronized void print() {
        try {
            System.out.println("wait");
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(FIELD[i][j] + "\t");
            }
            System.out.println();
        }


    }
}
