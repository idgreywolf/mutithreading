package com.levelup.examples;

public class Main {

    public static void main(String[] args) throws InterruptedException {


        Field field = new Field();


        new Thread(new Runnable() {
            @Override
            public void run() {
                field.print();
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                field.print();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                field.generate();
            }
        }).start();

    }
}

